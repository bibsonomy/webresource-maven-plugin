# webresource-maven-plugin

A Maven plugin to compress Javascript files (currently only [UglifyJS2](https://github.com/mishoo/UglifyJS2) is supported) and aggregate Javascript files. It can also compress CSS files ([YUI Compressor](http://yui.github.io/yuicompressor/)).

Based on [uglifyjs-maven-plugin](https://github.com/tqh/uglifyjs-maven-plugin) (Javascript compression) and [yuicompressor-maven-plugin](https://github.com/davidB/yuicompressor-maven-plugin) (aggregation, CSS compression).


## Repository

The plugin can be found in [our Maven repository](http://dev.bibsonomy.org/maven2).

To use our repository add the following to your `pom.xml`:


```
#!xml

<pluginRepositories>
  <pluginRepository>
    <id>bibsonomy-maven-plugin</id>
    <url>http://dev.bibsonomy.org/maven2</url>
  </pluginRepository>
</pluginRepositories>
```

## Installation

As this is a typical Maven plugin, simply declare the plugin in the `<plugins>` section of your `pom.xml` file:


```
#!xml

<plugin>
	<groupId>org.bibsonomy</groupId>
	<artifactId>webresource-maven-plugin</artifactId>
	<version>2.0.0</version>
	<executions>
		<execution>
			<id>webresource-compress-aggregation</id>
			<phase>compile</phase>
			<goals>
				<goal>compress-aggregate</goal>
			</goals>
			<configuration>
			[...]
			</configuration>
		</execution>
	</executions>
</plugin>
```


## Usage

### Compression

To compress Javascript and CSS files add the following to the configuration of the plugin:


```
#!xml

<compressions>
	<compression>
		<directory>path/to/resource/folder</directory>
		<excludes>
			<exclude>path/to/javascript.js</exclude>
			<exclude>path/to/css.css</exclude>
		</excludes>
		<includes>
			<include>**/*.js</include>
			<include>**/*.css</include>
		</includes>
		<copyExcludedFiles>true</copyExcludedFiles>
		<outputDirectory>${project.build.directory}/${project.build.finalName}/path/to/resource/folder</outputDirectory>
	</compression>
</compressions>
```

Parameter              | Description
-------------          | -------------
directory              | the path to the base directory of the Javascript/CSS files to compress
excludes               | a list of all files to exclude (each in a exclude element)
includes               | a list of all files to include (each in a include element)
copyExcludedFiles      | flag iff excluded Javascript/CSS files should be copied to the outputDirectory without compression


### Aggregation

To aggregate Javascript files, add the following to the configuration:


```
#!xml

<aggregations>
	<!-- javascript files for all pages -->
	<aggregation>
		<insertNewLine>true</insertNewLine>
		<output>path/to/folder/aggr.js</output>
		<includes>
			<include>path/to/folder/jsfile1.js</include>
			<include>path/to/folder/jsfile2.js</include>
		</includes>
	</aggregation>
</aggregations>

```

Parameter              | Description
-------------          | -------------
insertNewLine          | if true a new line is insert between each aggregated Javascript file
includes               | a list of files to include (each in a include element)
output                 | the path to the aggregated file 


### Notes

**Please note: ** You have to exclude the Javascript/CSS files from the war build:

```
#!xml

<plugin>
	<groupId>org.apache.maven.plugins</groupId>
	<artifactId>maven-war-plugin</artifactId>
	<version>2.1.1</version>
	<configuration>
	<!-- otherwise the Javascript/CSS files compressed by webresource-maven-plugin are overwritten by this plugin -->
		<warSourceExcludes>**/*.js,**/*.css</warSourceExcludes>
	</configuration>
</plugin>
```