package org.bibsonomy.webresource.compress.javascript;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.bibsonomy.webresource.compress.javascript.Uglify2Compressor;
import org.junit.Test;

/**
 * tests for {@link Uglify2Compressor}
 *
 * @author dzo
 */
public class Uglify2CompressorTest {
	
	private static final Uglify2Compressor COMPRESSOR = new Uglify2Compressor();
	
	/**
	 * tests {@link Uglify2Compressor#compress(String)}
	 * @throws IOException 
	 */
	@Test
	public void testCompress() throws IOException {
		assertEquals("function test(t){return t}", COMPRESSOR.compress("function test(a) {\n" + 
				"	return a;\n" + 
				"}"));
	}
}
