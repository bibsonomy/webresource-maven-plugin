package org.bibsonomy.webresource.compress.css;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;

/**
 * tests for {@link YuiCSSCompressor}
 * 
 * @author dzo
 */
public class YuiCSSCompressorTest {
	
	private static final YuiCSSCompressor COMPRESSOR = new YuiCSSCompressor();
	
	/**
	 * tests {@link YuiCSSCompressor#compress(String)}
	 * @throws IOException
	 */
	@Test
	public void testCompress() throws IOException {
		assertEquals("div{color:red}div{text-weight:bold}", COMPRESSOR.compress("div { color: red; }\n div { text-weight: bold; }"));
	}
}
