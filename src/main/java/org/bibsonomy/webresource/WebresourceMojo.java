package org.bibsonomy.webresource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;
import org.bibsonomy.webresource.compress.Compressor;
import org.bibsonomy.webresource.compress.css.YuiCSSCompressor;
import org.bibsonomy.webresource.compress.javascript.Uglify2Compressor;
import org.bibsonomy.webresource.util.Aggregation;
import org.bibsonomy.webresource.util.WebresourceFileSet;
import org.codehaus.plexus.util.Scanner;
import org.sonatype.plexus.build.incremental.BuildContext;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * compress and aggreate javascript files
 */
@Mojo(name = "compress-aggregate")
public class WebresourceMojo extends AbstractMojo {

	private static final Map<String, Compressor> COMPRESSORS = new HashMap<>();

	static {
		COMPRESSORS.put("css", new YuiCSSCompressor());
		COMPRESSORS.put("js", new Uglify2Compressor());
	}

	@Component
	private BuildContext context;

	@Parameter(defaultValue = "UTF-8")
	private String encoding;

	/**
	 * {@link WebresourceFileSet} containing JavaScript and CSS source files.
	 */
	@Parameter
	private List<WebresourceFileSet> compressions;

	@Parameter
	private List<Aggregation> aggregations;

	/**
	 * number of threads to use to compress the compression files
	 */
	@Parameter(defaultValue = "0")
	private int numberOfThreads;

	private static File getOutputFile(File inputFile, final FileSet fileSet) throws IOException {
		final String relativePath = getSourceDir(fileSet).toURI().relativize(inputFile.getParentFile().toURI()).getPath();
		final File outputBaseDir = new File(fileSet.getOutputDirectory(), relativePath);
		if (!outputBaseDir.exists()) {
			FileUtils.forceMkdir(outputBaseDir);
		}
		return new File(outputBaseDir, inputFile.getName());
	}

	/**
	 * Returns {@link File directory} containing JavaScript source {@link File files}.
	 *
	 * @param fileset
	 * @return {@link File Directory} containing JavaScript source {@link File files}
	 */
	private static File getSourceDir(FileSet fileset) {
		return new File(fileset.getDirectory());
	}

	/**
	 * Returns JavaScript source {@link File files}.
	 *
	 * @param buildContext
	 * @param compression
	 * @return Array of JavaScript source {@link File files}
	 */
	private static List<File> getSourceFiles(final BuildContext buildContext, final WebresourceFileSet compression) {
		final File sourceDir = getSourceDir(compression);
		final File targetDir = new File(compression.getOutputDirectory());

		// build scanner to get source files
		final Scanner scanner = buildContext.newScanner(sourceDir);
		scanner.setExcludes(compression.getExcludesArray());
		scanner.setIncludes(compression.getIncludesArray());
		scanner.scan();

		final List<File> files = new LinkedList<>();
		for (final String name : scanner.getIncludedFiles()) {
			// check if file is up to date
			final File sourceFile = new File(sourceDir, name);
			final File targetFile = new File(targetDir, name);

			final boolean uptodate = buildContext.isUptodate(targetFile, sourceFile);

			if (!uptodate) {
				files.add(sourceFile);
			}
		}
		return files;
	}

	@Override
	public void execute() throws MojoExecutionException {
		try {
			if (this.compressions != null) {
				for (final WebresourceFileSet compression : this.compressions) {
					if (compression.isCopyExcludedFiles()) {
						this.getLog().info("Copying excluded files");
						this.copyExcludedFiles(compression);
					}
					final List<File> filesToCompress = getSourceFiles(this.context, compression);
					this.getLog().info("Compressing " + filesToCompress.size() + " file(s).");
					final ExecutorService executorService = Executors.newFixedThreadPool(
									numberOfThreads <= 0 ? Runtime.getRuntime().availableProcessors() - 1 : numberOfThreads);
					for (File resourceFile : filesToCompress) {
						executorService.execute(new CompressionRunnable(compression, resourceFile));
					}
					executorService.shutdown();
					executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
				}
				this.getLog().info("Finished compressing.");
			}
			if (this.aggregations != null) {
				getLog().info("Aggregate files");
				this.aggregateFiles();
			}
		} catch (final IOException | InterruptedException e) {
			throw new MojoExecutionException("Failure.", e);
		}
	}

	/**
	 * @throws IOException
	 */
	private void aggregateFiles() throws IOException {
		for (final Aggregation aggregation : this.aggregations) {
			final File outputFile = aggregation.getOutput();
			getLog().info("Building aggregation " + outputFile.getPath());

			// create base folder
			final File folder = outputFile.getParentFile();
			folder.mkdirs();

			// delete old file if exists
			if (outputFile.exists()) {
				outputFile.delete();
			}

			final OutputStream out = this.context.newFileOutputStream(outputFile);

			final List<String> includes = Arrays.asList(aggregation.getIncludes());
			final Iterator<String> includesIterator = includes.iterator();
			while (includesIterator.hasNext()) {
				final String include = includesIterator.next();
				final File file = new File(include);

				getLog().info(" - adding " + file.getPath());

				try (FileInputStream fileInputStream = new FileInputStream(file)) {
					IOUtils.copy(fileInputStream, out);

					if (includesIterator.hasNext() && aggregation.isInsertNewLine()) {
						out.write('\n');
					}
				}
			}

			out.close();
		}
	}

	private void copyExcludedFiles(final FileSet fileset) {
		final FileSetManager fileSetManager = new FileSetManager();
		final String[] excludedPaths = fileSetManager.getExcludedFiles(fileset);
		for (final String excludedPath : excludedPaths) {
			final File excludedFile = new File(getSourceDir(fileset), excludedPath);

			this.getLog().info("Copying " + excludedFile.getPath());

			try {
				final FileInputStream input = new FileInputStream(excludedFile);
				final OutputStream output = this.context.newFileOutputStream(getOutputFile(excludedFile, fileset));
				IOUtils.copy(input, output);
				IOUtils.closeQuietly(input);
				IOUtils.closeQuietly(output);
			} catch (final IOException e) {
				getLog().error("error copying file", e);
			}
		}
	}

	/**
	 * @param file the file to compress
	 * @return the compressed content
	 * @throws IOException
	 */
	protected String compressFile(final File file) throws IOException {
		final String data = FileUtils.readFileToString(file, this.encoding);

		final String extension = FilenameUtils.getExtension(file.getName());
		return COMPRESSORS.get(extension).compress(data);
	}

	private final class CompressionRunnable implements Runnable {
		final FileSet fileSet;
		final File resourceFile;

		private CompressionRunnable(FileSet fileSet, File resourceFile) {
			this.fileSet = fileSet;
			this.resourceFile = resourceFile;
		}

		@Override
		public void run() {
			final String resourceFilePath = resourceFile.getPath();
			WebresourceMojo.this.getLog().info("Compressing " + resourceFilePath);
			try {
				final String compressedContent = compressFile(resourceFile);
				final File outputFile = getOutputFile(resourceFile, fileSet);
				try (final BufferedOutputStream out = new BufferedOutputStream(
								WebresourceMojo.this.context.newFileOutputStream(outputFile))) {
					out.write(compressedContent.getBytes(WebresourceMojo.this.encoding));
					WebresourceMojo.this.getLog().info("Compressed " + resourceFilePath);
					out.flush();
				} catch (final IOException e) {
					getLog().error("Could not compress " + resourceFile.getPath() + ".", e);
				}
			} catch (IOException e) {
				getLog().error("Could not compress " + resourceFile.getPath() + ".", e);
			}
		}
	}
}
