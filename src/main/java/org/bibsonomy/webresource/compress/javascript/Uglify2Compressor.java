package org.bibsonomy.webresource.compress.javascript;

import org.apache.commons.io.IOUtils;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.bibsonomy.webresource.compress.JavascriptCompressor;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.ScriptableObject;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * a {@link JavascriptCompressor} that uses Uglify2 to compress javascript files
 *
 * @author dzo
 */
public class Uglify2Compressor implements JavascriptCompressor {

	private static final Log LOG = new SystemStreamLog();
	/**
	 * the path to the uglify js 2 lib
	 */
	private static final String UGLIFY_2_PATH = "UglifyJS2/";

	/* (non-Javadoc)
	 * @see org.bibsonomy.javascript.compress.JavascriptCompressor#compress(java.lang.String)
	 */
	@Override
	public String compress(String content) {
		try {
			final JavascriptContext javascriptContext = new JavascriptContext();

			javascriptContext.load("adapter.js", UGLIFY_2_PATH + "utils.js", UGLIFY_2_PATH + "ast.js",
							UGLIFY_2_PATH + "transform.js", UGLIFY_2_PATH + "compress.js", UGLIFY_2_PATH + "mozilla-ast.js",
							UGLIFY_2_PATH + "output.js", UGLIFY_2_PATH + "parse.js", UGLIFY_2_PATH + "propmangle.js",
							UGLIFY_2_PATH + "scope.js", UGLIFY_2_PATH + "sourcemap.js", "uglifyJavascript.js");

			return javascriptContext.executeCmdOnFile("uglifyJavascript", content);

		} catch (IOException e) {
			LOG.error("Error while compressing file.", e);
		} finally {
			Context.exit();
		}

		return null;
	}

	class JavascriptContext {
		private final Context cx = Context.enter();
		private final ScriptableObject global = cx.initStandardObjects();

		void load(String... scripts) throws IOException {
			final ClassLoader cl = getClass().getClassLoader();
			for (final String script : scripts) {
				final InputStreamReader in = new InputStreamReader(cl.getResourceAsStream("script/uglifyJS/" + script));
				cx.evaluateReader(this.global, in, script, 1, null);
				IOUtils.closeQuietly(in);
			}
		}

		String executeCmdOnFile(String cmd, String data) {
			ScriptableObject.putProperty(this.global, "data", data);
			return cx.evaluateString(this.global,
							cmd + "(String(data));", "<cmd>", 1, null).toString();
		}
	}

}
