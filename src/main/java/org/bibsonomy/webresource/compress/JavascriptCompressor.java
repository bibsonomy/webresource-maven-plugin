package org.bibsonomy.webresource.compress;

/**
 * compresses a javascript file
 * @author dzo
 */
public interface JavascriptCompressor extends Compressor {
	// noop
}
