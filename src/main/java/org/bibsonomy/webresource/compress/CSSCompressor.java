package org.bibsonomy.webresource.compress;

/**
 * compresses a css file
 * @author dzo
 */
public interface CSSCompressor extends Compressor {
	// noop
}
