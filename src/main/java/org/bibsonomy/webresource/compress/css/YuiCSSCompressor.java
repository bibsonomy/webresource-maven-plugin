package org.bibsonomy.webresource.compress.css;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import org.bibsonomy.webresource.compress.CSSCompressor;

import com.yahoo.platform.yui.compressor.CssCompressor;

/**
 * wrapper for {@link CssCompressor}
 *
 * @author dzo
 */
public class YuiCSSCompressor implements CSSCompressor {

	/* (non-Javadoc)
	 * @see org.bibsonomy.webresource.compress.CSSCompressor#compress(java.lang.String)
	 */
	@Override
	public String compress(String content) throws IOException {
		final CssCompressor compressor = new CssCompressor(new StringReader(content));
		final StringWriter writer = new StringWriter();
		compressor.compress(writer, -1);
		return writer.toString();
	}

}
