package org.bibsonomy.webresource.compress;

import java.io.IOException;

import com.yahoo.platform.yui.compressor.CssCompressor;

/**
 * a interface for both {@link JavascriptCompressor} and {@link CssCompressor}
 *
 * @author dzo
 */
public interface Compressor {
	/**
	 * compresses the content 
	 * @param content
	 * @return the compressed content
	 * @throws IOException
	 */
	String compress(final String content) throws IOException;
}
