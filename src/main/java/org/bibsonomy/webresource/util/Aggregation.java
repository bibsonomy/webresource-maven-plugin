package org.bibsonomy.webresource.util;

import java.io.File;

import org.apache.maven.plugins.annotations.Parameter;

/**
 * class to represent a aggreation
 * @author dzo
 */
public class Aggregation {
	@Parameter
	private boolean insertNewLine;
	
	@Parameter
	private File output;
	
	@Parameter
	private String[] includes;

	/**
	 * @return the insertNewLine
	 */
	public boolean isInsertNewLine() {
		return this.insertNewLine;
	}

	/**
	 * @param insertNewLine the insertNewLine to set
	 */
	public void setInsertNewLine(boolean insertNewLine) {
		this.insertNewLine = insertNewLine;
	}

	/**
	 * @return the output
	 */
	public File getOutput() {
		return this.output;
	}

	/**
	 * @param output the output to set
	 */
	public void setOutput(File output) {
		this.output = output;
	}

	/**
	 * @return the includes
	 */
	public String[] getIncludes() {
		return this.includes;
	}

	/**
	 * @param includes the includes to set
	 */
	public void setIncludes(String[] includes) {
		this.includes = includes;
	}
}
