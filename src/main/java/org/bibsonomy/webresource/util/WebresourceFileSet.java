package org.bibsonomy.webresource.util;

import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.model.fileset.FileSet;

/**
 * a {@link FileSet} extension to control if excluded files should be copied
 *
 * @author dzo
 */
public class WebresourceFileSet extends FileSet {
	private static final long serialVersionUID = 474091699656892123L;
	
	@Parameter
	private boolean copyExcludedFiles;

	/**
	 * @return the copyExcludedFiles
	 */
	public boolean isCopyExcludedFiles() {
		return this.copyExcludedFiles;
	}

	/**
	 * @param copyExcludedFiles the copyExcludedFiles to set
	 */
	public void setCopyExcludedFiles(boolean copyExcludedFiles) {
		this.copyExcludedFiles = copyExcludedFiles;
	}
}
